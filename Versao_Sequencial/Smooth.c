#include<stdio.h>
#include<stdlib.h>

#include"Smooth.h"

int **R, **G, **B;
int dimL, dimH;
char type_file[4];
char valueMax[5];

void start(){
	FILE *file_in;
	FILE *file_out;
	
	/* Abre os arquivos de input e output */
	if(!(file_in = fopen("imagem2.ppm", "r"))){
		printf("Erro na abertura do arquivo in.ppm\n\n");
		exit(EXIT_FAILURE);
	}
	
	if(!(file_out = fopen("out.ppm", "w"))){
		printf("Erro na abertura do arquivo out.ppm\n\n");
		exit(EXIT_FAILURE);
	}
	
	read_file(file_in);	
	
/*	printMatrix();
*/
	/* Escreve o cabeçalho 
	  no arquivo de saida */
	write_header(file_out);
	
	if(dimH >= 5 && dimL >= 5)
		smooth_five(file_out);	

	free_matrix(dimH, dimL);
	
	fclose(file_in);
	fclose(file_out);	
}

void smooth_five(FILE *arq){
	int i, j;
	int m, n;
	int soma;
	
	for(i = 0; i < dimH; i++){
		for(j = 0; j < dimL; j++){
						
			/* Smooth matrix Red */
			soma = 0;
			for(m = i-2; m <= i + 2; m++){
				for(n = j-2; n <= j + 2; n++){
					if(m < 0 || n < 0 || m > dimH-1 || n > dimL-1)
						continue;					
					soma += R[m][n];
				}
			}
			soma = soma / 25;			
			fprintf(arq, "%d ", soma);
			
			/* Smooth matrix Green */
			soma = 0;
			for(m = i-2; m <= i + 2; m++){
				for(n = j-2; n <= j + 2; n++){
					if(m < 0 || n < 0 || m > dimH-1 || n > dimL-1)
						continue;					
					soma += G[m][n];
				}
			}
			soma = soma / 25;			
			fprintf(arq, "%d ", soma);
			
			/* Smooth matrix Blue */
			soma = 0;
			for(m = i-2; m <= i + 2; m++){
				for(n = j-2; n <= j + 2; n++){
					if(m < 0 || n < 0 || m > dimH-1 || n > dimL-1)
						continue;					
					soma += B[m][n];
				}
			}
			soma = soma / 25;			
			fprintf(arq, "%d\n", soma);
		}
	}
}

/* Le o arquivo e carrega as matrizes RGB */
void read_file(FILE *arq){	
	int i, j;
	
	/* Le o formato do arquivo */
	fgets(type_file, sizeof(type_file), arq);	
	
	/* Le as dimensoes da imagem */
	fscanf(arq,"%d %d\n", &dimH, &dimL);	
	
	/* Aloca as matrizes RGB */
	aloca_matrix(dimH, dimL);
	
	/* Le o valor maximo da cor */
	fgets(valueMax, sizeof(valueMax), arq);	
		
	/* Preenche as matrizes RGB */
	for(i = 0; i < dimH; i++){
		for(j = 0; j < dimL; j++){			
			fscanf(arq,"%d", &R[i][j]);
			fscanf(arq,"%d", &G[i][j]);
			fscanf(arq,"%d", &B[i][j]);			
		}
	}
}

/* Escreve tipo do arquivo, dimensoes e valor
maximo de valores RGB no arquivo de saida */
void write_header(FILE *arq){
	fprintf(arq, "%s", type_file);
	fprintf(arq, "%d %d\n", dimH, dimL);
	fprintf(arq, "%s", valueMax);
}

/* Aloca as matrizes R G B */
void aloca_matrix(){
	int i;

	/* Matriz Red */
	R = (int**) malloc (dimH * sizeof(int*));
	if(R == NULL){
		printf("Erro na alocação\n");
		exit(1);
	}
	for(i = 0; i < dimH ; i++){
		R[i] = (int*)malloc(dimL * sizeof(int));
		if(R[i] == NULL){
			printf("Erro de alocação de memoria\n");
			exit(1);
		}
	}
	
	/* Matriz Green */
	G = (int**) malloc (dimH * sizeof(int*));
	if(G == NULL){
		printf("Erro na alocação\n");
		exit(1);
	}
	for(i = 0; i < dimH ; i++){
		G[i] = (int*)malloc(dimL * sizeof(int));
		if(G[i] == NULL){
			printf("Erro de alocação de memoria\n");
			exit(1);
		}
	}
	
	/* Matriz Blue */
	B = (int**) malloc (dimH * sizeof(int*));
	if(B == NULL){
		printf("Erro na alocação\n");
		exit(1);
	}
	for(i = 0; i < dimH ; i++){
		B[i] = (int*)malloc(dimL * sizeof(int));
		if(B[i] == NULL){
			printf("Erro de alocação de memoria\n");
			exit(1);
		}
	}
}

/* Libera a memoria das matrizes RGB */
void free_matrix(){
	int i;
	
	for(i = 0; i < dimH; i++){
		free(R[i]);
	}
	free(R);
	
	for(i = 0; i < dimH; i++){
		free(G[i]);
	}
	free(G);
	
	for(i = 0; i < dimH; i++){
		free(B[i]);
	}
	free(B);
}

/* Imprime as matrizes, função debuging */
void printMatrix(){
	int i, j;
	printf("\nRED:\n");
	for(i = 0; i < dimH; i++){
		for(j = 0; j < dimL; j++){
			printf("%d ", R[i][j]);
		}
		printf("\n");
	}
	printf("\nGREEN:\n");
	for(i = 0; i < dimH; i++){
		for(j = 0; j < dimL; j++){
			printf("%d ", G[i][j]);
		}
		printf("\n");
	}
	printf("\nBLUE:\n");
	for(i = 0; i < dimH; i++){
		for(j = 0; j < dimL; j++){
			printf("%d ", B[i][j]);
		}
		printf("\n");
	}
	
}