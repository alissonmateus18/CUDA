
#ifndef _IMAGE_PPM_H_
#define _IMAGE_PPM_H_

#define SAVE_MODE_TEXT      0
#define SAVE_MODE_BINARY    1

#define COLOR_RED   1
#define COLOR_GREEN 2
#define COLOR_BLUE  3

typedef struct {
	void *r, *g, *b;
	int width, height;
	char colorsize;
} Image;

// Free all memory used by the image
void release_image(Image *img);

// Loads an image from file
Image* load_image(const char* file);

// Loads an image from file adding a border
Image* load_imageWithBorder(const char* file, int border);

// Returns the image channel
void* getImageChannel(Image *img, int rgb);

// Save as an image file
// mode: 0 = text, 1 = binary
void save_image(Image *img, const char* file, int mode);

#endif
