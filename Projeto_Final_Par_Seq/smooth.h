
#ifndef _SMOOTH_H_
#define _SMOOTH_H_

#include "imagerect.h"

ImageRect* smooth(ImageRect *imgRect, int smooth_size);

// Adiciona uma borda na imagem
ImageRect* makeBorder(ImageRect *imgRect, int border_size, unsigned char color);


#endif // _SMOOTH_H_
