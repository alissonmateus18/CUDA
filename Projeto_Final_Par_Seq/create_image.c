
#include <stdio.h>
#include <stdlib.h>

/**
	argv[1] Nome do arquivo a ser criado
	argv[2] Image width
	argv[3] Image height
*/
int main(int argc, char *argv[]){

	int i;

	if (argc != 4)
	{
		printf("Parâmetros Inválidos! Program file width height\n");
	}

	FILE *f = fopen(argv[1], "wb");
	
	int width = atoi(argv[2]);
	int height = atoi(argv[3]);
	
	if (f == NULL){
		printf("Não foi possível abrir o arquivo para escrita! %s\n", argv[1]);
		return;
	}

	fprintf(f,"P6 %d %d %d ", width, height, 255);

	char rgb[3];
	for (i=0; i < width * height; ++i){
		rgb[0] = i%255;
		rgb[1] = ((int)(width*(0.2*(float)(i%10))))%255;
		rgb[2] = (i/height)%255;
		fwrite(rgb, 3, 1, f );
	}
	
	fclose(f);

}
