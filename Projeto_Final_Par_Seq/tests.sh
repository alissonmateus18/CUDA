

sequencial() {

	N=$1
	FILEIN=$2
	FILEOUT=$3
	FILEOUT_TIME=$4

	TEST_INFO="Running $N sequential tests IN: $FILEIN OUT: $FILEOUT_TIME"
	
	echo StartTime >> $FILEOUT_TIME
	date >> $FILEOUT_TIME
	echo $TEST_INFO >> $FILEOUT_TIME
	
	for ((c=1; c<=N; c++))
	do
		echo $c: $TEST_INFO
		./smoother_seq $FILEIN $FILEOUT >> $FILEOUT_TIME
	done

	echo EndTime >> $FILEOUT_TIME
	date >> $FILEOUT_TIME
	echo "####################################################################################" >> $FILEOUT_TIME
}

mpi() {

	N=$1
	FILEIN=$2
	FILEOUT=$3
	FILEOUT_TIME=$4
	NTHREADS=$5
	
	TEST_INFO="Running $N mpi tests IN: $FILEIN OUT: $FILEOUT_TIME THREADS: $NTHREADS"

	echo StartTime >> $FILEOUT_TIME
	date >> $FILEOUT_TIME
	echo $TEST_INFO >> $FILEOUT_TIME
	
	for ((c=1; c<=N; c++))
	do
		echo $c: $TEST_INFO
		if [ "$NTHREADS" -eq "-1" ]; then
			#echo use max
			mpirun -hostfile hosts ./smoother_mpi $FILEIN $FILEOUT >> $FILEOUT_TIME
		else
			#echo use np
			mpirun -hostfile hosts -np $NTHREADS ./smoother_mpi $FILEIN $FILEOUT >> $FILEOUT_TIME
		fi
	done

	echo EndTime >> $FILEOUT_TIME
	date >> $FILEOUT_TIME
	echo "####################################################################################" >> $FILEOUT_TIME
}

cuda() {

	N=$1
	FILEIN=$2
	FILEOUT=$3
	FILEOUT_TIME=$4
	NTHREADS=$5

	TEST_INFO="Running $N cuda tests IN: $FILEIN OUT: $FILEOUT_TIME THREADS: $NTHREADS"
	
	echo StartTime >> $FILEOUT_TIME
	date >> $FILEOUT_TIME
	echo $TEST_INFO >> $FILEOUT_TIME
	
	for ((c=1; c<=N; c++))
	do
		echo $c: $TEST_INFO
		./smoother_cuda $FILEIN $FILEOUT $NTHREADS >> $FILEOUT_TIME
	done

	echo EndTime >> $FILEOUT_TIME
	date >> $FILEOUT_TIME
	echo "####################################################################################" >> $FILEOUT_TIME
}

create_input(){
	make img
	./makeimg img4k.ppm 4000 4000
	./makeimg img8k.ppm 8000 8000
	./makeimg img13k.ppm 13000 13000
}

# SEQUENCIAL
test_seq(){
	make seq

	# num_tests ImageIn ImageOut TimeFile
	sequencial 10 img4k.ppm out_seq.ppm exectimes_seq.txt
	sequencial 10 img8k.ppm out_seq.ppm exectimes_seq.txt
	sequencial 10 img13k.ppm out_seq.ppm exectimes_seq.txt
	
}

# MPI
test_mpi(){
	make mpi

	# num_tests Image ImageOut TimeFile threads
	
	# 4k
	mpi 10 img4k.ppm out_mpi.ppm exectimes_mpi.txt 48
	mpi 10 img4k.ppm out_mpi.ppm exectimes_mpi.txt 36
	mpi 10 img4k.ppm out_mpi.ppm exectimes_mpi.txt 18
	
	# 8k
	mpi 10 img8k.ppm out_mpi.ppm exectimes_mpi.txt 48
	mpi 10 img8k.ppm out_mpi.ppm exectimes_mpi.txt 36
	mpi 10 img8k.ppm out_mpi.ppm exectimes_mpi.txt 18
	
	# 13k
	mpi 10 img13k.ppm out_mpi.ppm exectimes_mpi.txt 48
	mpi 10 img13k.ppm out_mpi.ppm exectimes_mpi.txt 36
	mpi 10 img13k.ppm out_mpi.ppm exectimes_mpi.txt 18

}

# MPI
test_cuda(){
	make cuda

	# num_tests Image ImageOut TimeFile threads
	cuda 10 img4k.ppm out_cuda.ppm exectimes_cuda.txt 1024
	cuda 10 img8k.ppm out_cuda.ppm exectimes_cuda.txt 1024
	cuda 10 img13k.ppm out_cuda.ppm exectimes_cuda.txt 1024
	
	cuda 10 img4k.ppm out_cuda.ppm exectimes_cuda.txt 512
	cuda 10 img8k.ppm out_cuda.ppm exectimes_cuda.txt 512
	cuda 10 img13k.ppm out_cuda.ppm exectimes_cuda.txt 512
	
	cuda 10 img4k.ppm out_cuda.ppm exectimes_cuda.txt 333
	cuda 10 img8k.ppm out_cuda.ppm exectimes_cuda.txt 333
	cuda 10 img13k.ppm out_cuda.ppm exectimes_cuda.txt 333

}


############################# Definir execucao
create_input

mpirun -hostfile hosts ./smoother_mpi img8k.ppm out_mpi.ppm 

test_mpi