
#ifndef _IMAGE_RECT_H_
#define _IMAGE_RECT_H_

#include "imageppm.h"

typedef struct {
    void *data; //! Dados da imagem
    int width; //! Tamanho real da matriz
    int height; //! Tamanho real da matriz
    int offsetX; //! Offset da matriz original
    int offsetY; //! Offset da matriz original
    char colorsize; //! Numero de bytes por pixel
    char color; //! R G B
} ImageRect;

void release_imageRect(ImageRect* ptr);

ImageRect* CreateImageRect(Image *img, int RGB, int offX, int offY, int w, int h);

Image* transform_into_image(ImageRect* imgrect);

#endif