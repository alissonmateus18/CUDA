
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "imageppm.h"
#include "smooth.h"

/**
 * argv[0] - Program name
 * argv[1] - Input image file name
 */
int main(int argc, char *argv[])
{
	struct timeval start_all, end_all;
    	struct timeval start_seq, end_seq;
    	gettimeofday(&start_all, NULL);

	ImageRect *result[3];
	ImageRect RGB[3];
	Image imgResult;
	Image *img;

	int smooth_size = 2;

	// LOAD START
	img = load_image(argv[1]);

	// R
	RGB[0].width = img->width;
	RGB[0].height = img->height;
	RGB[0].colorsize = img->colorsize;
	RGB[0].color = COLOR_RED;
	RGB[0].data = img->r;

	// G
	RGB[1].width = img->width;
	RGB[1].height = img->height;
	RGB[1].colorsize = img->colorsize;
	RGB[1].color = COLOR_GREEN;
	RGB[1].data = img->g;

	// B
	RGB[2].width = img->width;
	RGB[2].height = img->height;
	RGB[2].colorsize = img->colorsize;
	RGB[2].color = COLOR_BLUE;
	RGB[2].data = img->b;

	// LOAD END

	gettimeofday(&start_seq, NULL);

	// START PROCESSING

	result[0] = smooth(&RGB[0], smooth_size);
	result[1] = smooth(&RGB[1], smooth_size);
	result[2] = smooth(&RGB[2], smooth_size);

	// END PROCESSING

	gettimeofday(&end_seq, NULL);

	// START RETURNING RESULT

	imgResult.width = img->width;
	imgResult.height = img->height;
	imgResult.colorsize = img->colorsize;
	imgResult.r = result[0]->data;
	imgResult.g = result[1]->data;
	imgResult.b = result[2]->data;

	save_image(&imgResult, argv[2], SAVE_MODE_BINARY);

	release_imageRect(result[0]);
	release_imageRect(result[1]);
	release_imageRect(result[2]);

	// END RESULT

	gettimeofday(&end_all, NULL);

	double result_all = end_all.tv_sec - start_all.tv_sec + (end_all.tv_usec - start_all.tv_usec) / 1000000.0;
   	double result_seq = end_seq.tv_sec - start_seq.tv_sec + (end_seq.tv_usec - start_seq.tv_usec) / 1000000.0;
	double result_load = start_seq.tv_sec - start_all.tv_sec + (start_seq.tv_usec - start_all.tv_usec) / 1000000.0;
	double result_ret = end_all.tv_sec - end_seq.tv_sec + (end_all.tv_usec - end_seq.tv_usec) / 1000000.0;

    	printf("%lf %lf %lf %lf\n", result_load, result_ret, result_seq, result_all);

	return 0;
}
